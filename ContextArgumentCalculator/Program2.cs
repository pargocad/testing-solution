﻿﻿﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.CSharp;

public class ArgumentCalculatorTester
{
    static IDictionary<string, object> argument_final_value = new Dictionary<string, object>()
        {
            {"min_M", 1000},
            {"max_M", 50000},
            {"min_N", 1000},
            {"max_N", 50000},
            {"min_P", 1000},
            {"max_P", 50000},
            {"platform-maintainer", "br.ufc.mdcc.hpcshelf.platform.Maintainer"},
            {"platform-virtual", "br.ufc.mdcc.common.YesOrNo"},
            {"platform-dedicated", "br.ufc.mdcc.common.YesOrNo"},
            {"platform-node-count", 8},
            {"platform-node-locale", "br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere"},
            {"platform-node-power_consumption", (decimal)4.5},
            {"platform-node-cost_per_hour", (decimal)1.3},
            {"platform-node-processor-count", 1},
            {"platform-node-processor-manufacturer", "br.ufc.mdcc.hpcshelf.platform.node.processor.Manufacturer"},
            {"platform-node-processor-family", "br.ufc.mdcc.hpcshelf.platform.node.processor.Family"},
            {"platform-node-processor-series", "br.ufc.mdcc.hpcshelf.platform.node.processor.Series"},
            {"platform-node-processor-microarchitecture", "br.ufc.mdcc.hpcshelf.platform.node.processor.Microarchitecture"},
            {"platform-node-processor-model", "br.ufc.mdcc.hpcshelf.platform.node.processor.Model"},
            {"platform-node-processor-core-count", 1},
            {"platform-node-processor-core-clock", 0},
            {"platform-node-processor-core-threads_per_core", 1},
            {"platform-node-processor-core-cache_L1i-mapping", "br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping"},
            {"platform-node-processor-core-cache_L1i-size", 0},
            {"platform-node-processor-core-cache_L1i-latency", int.MaxValue},
            {"platform-node-processor-core-cache_L1i-line_size", 0},
            {"platform-node-processor-core-cache_L1i", "br.ufc.mdcc.hpcshelf.platform.node.processor.Cache"},
            {"platform-node-processor-core-cache_L1d-mapping", "br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping"},
            {"platform-node-processor-core-cache_L1d-size", 0},
            {"platform-node-processor-core-cache_L1d-latency", int.MaxValue},
            {"platform-node-processor-core-cache_L1d-line_size", 0},
            {"platform-node-processor-core-cache_L1d", "br.ufc.mdcc.hpcshelf.platform.node.processor.Cache"},
            {"platform-node-processor-core-cache_L2-mapping", "br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping"},
            {"platform-node-processor-core-cache_L2-size", 0},
            {"platform-node-processor-core-cache_L2-latency", int.MaxValue},
            {"platform-node-processor-core-cache_L2-line_size", 0},
            {"platform-node-processor-core-cache_L2", "br.ufc.mdcc.hpcshelf.platform.node.processor.Cache"},
            {"platform-node-processor-core", "br.ufc.mdcc.hpcshelf.platform.node.processor.Core"},
            {"platform-node-processor-cache_L3-mapping", "br.ufc.mdcc.hpcshelf.platform.node.processor.cache.Mapping"},
            {"platform-node-processor-cache_L3-size", 0},
            {"platform-node-processor-cache_L3-latency", int.MaxValue},
            {"platform-node-processor-cache_L3-line_size", 0},
            {"platform-node-processor-cache_L3", "br.ufc.mdcc.hpcshelf.platform.node.processor.Cache"},
            {"platform-node-processor-t_sFMA", decimal.MaxValue},
            {"platform-node-processor-t_dFMA", decimal.MaxValue},
            {"platform-node-processor-t_iADD", decimal.MaxValue},
            {"platform-node-processor-t_iSUB", decimal.MaxValue},
            {"platform-node-processor-t_iMUL", decimal.MaxValue},
            {"platform-node-processor-t_iDIV", decimal.MaxValue},
            {"platform-node-processor-t_sADD", decimal.MaxValue},
            {"platform-node-processor-t_sSUB", decimal.MaxValue},
            {"platform-node-processor-t_sMUL", decimal.MaxValue},
            {"platform-node-processor-t_sDIV", decimal.MaxValue},
            {"platform-node-processor-t_dADD", decimal.MaxValue},
            {"platform-node-processor-t_dSUB", decimal.MaxValue},
            {"platform-node-processor-t_dMUL", decimal.MaxValue},
            {"platform-node-processor-t_dDIV", decimal.MaxValue},
            {"platform-node-processor", "br.ufc.mdcc.hpcshelf.platform.node.Processor"},
            {"platform-node-accelerator-count", 1},
            {"platform-node-accelerator-manufacturer", "br.ufc.mdcc.hpcshelf.platform.node.accelerator.Manufacturer"},
            {"platform-node-accelerator-type", "br.ufc.mdcc.hpcshelf.platform.node.accelerator.Type"},
            {"platform-node-accelerator-architecture", "br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture"},
            {"platform-node-accelerator-model", "br.ufc.mdcc.hpcshelf.platform.node.accelerator.Model"},
            {"platform-node-accelerator-memory_size", 0},
            {"platform-node-accelerator", "br.ufc.mdcc.hpcshelf.platform.node.Accelerator"},
            {"platform-node-memory-size", 0},
            {"platform-node-memory-latency", int.MaxValue},
            {"platform-node-memory-bandwidth", 0},
            {"platform-node-memory", "br.ufc.mdcc.hpcshelf.platform.node.Memory"},
            {"platform-node-storage-latency", int.MaxValue},
            {"platform-node-storage-size", 0},
            {"platform-node-storage-bandwidth", 0},
            {"platform-node-storage-network_bandwidth", 0},
            {"platform-node-storage", "br.ufc.mdcc.hpcshelf.platform.node.Storage"},
            {"platform-node-operating_system", "br.ufc.mdcc.hpcshelf.platform.node.OS"},
            {"platform-node", "br.ufc.mdcc.hpcshelf.platform.Node"},
            {"platform-interconnection-startup_time", int.MaxValue},
            {"platform-interconnection-node_latency", int.MaxValue},
            {"platform-interconnection-bandwidth", 0},
            {"platform-interconnection-network_topology", "br.ufc.mdcc.hpcshelf.platform.interconnection.Topology"},
            {"platform-interconnection", "br.ufc.mdcc.hpcshelf.platform.Interconnection"},
            {"platform-storage-size", 0},
            {"platform-storage-latency", int.MaxValue},
            {"platform-storage-bandwidth", 0},
            {"platform-storage-network_bandwidth", 0},
            {"platform-storage", "br.ufc.mdcc.hpcshelf.platform.node.Storage"},
            {"platform-performance-peak_flops", 0},
            {"platform-performance-hpl_flops", 0},
            {"platform-performance-hpcg_flops", 0},
            {"platform-performance", "br.ufc.mdcc.hpcshelf.platform.Performance"},
            {"platform-power-hpl_total", int.MaxValue},
            {"platform-power-hpcg_total", -2147483648},
            {"platform-power-hpl_efficiency", int.MaxValue},
            {"platform-power-hpcg_efficiency", -2147483648},
            {"platform-power", "br.ufc.mdcc.hpcshelf.platform.Power"},
            {"platform", "br.ufc.mdcc.hpcshelf.platform.Platform"}
        };

    public static void Main(string[] argument)
    {
        var c = new ContextArgumentCalculator();
        c.Argument = argument_final_value;

        int ? t = c.execution_time;
        argument_final_value["execution_time"] = t;

        decimal ? u = c.energy_consumption;
        argument_final_value["power_consumption"] = u;

        decimal? v = c.monetary_cost;
        argument_final_value["monetary_cost"] = v;

        Console.WriteLine("execution time is {0}", t);
        Console.WriteLine("power consumption is {0}", u);
        Console.WriteLine("monetary cost is {0}", v);

        // object t = (new Teste()).calculate_quantifier_value();
        // Console.WriteLine("time is {0}", t);
    }


    class Teste
    {


        public object calculate_quantifier_value()
        {
            string class_id = "ContextArgumentCalculator";
            string sourceCode = "using System;\nusing System.Collections.Generic;\n\npublic class ContextArgumentCalculator\n{\n    public IDictionary<string, object> argument;\n    public IDictionary<string, object> Argument { set { argument = value; } }\n\n    // ASSUMING PRE-DEFINED TOPOLOGY\n    IDictionary<int, Tuple<int, int>> topology = new Dictionary<int, Tuple<int, int>>()\n                {\n                    { 1,  new Tuple<int, int>(1, 1) },\n                    { 2,  new Tuple<int, int>(1, 2) },\n                    { 4,  new Tuple<int, int>(2, 2) },\n                    { 6,  new Tuple<int, int>(3, 2) },\n                    { 8,  new Tuple<int, int>(4, 2) },\n                    { 9,  new Tuple<int, int>(3, 3) },\n                    { 10, new Tuple<int, int>(5, 2) },\n                    { 12, new Tuple<int, int>(4, 3) },\n                    { 14, new Tuple<int, int>(7, 2) },\n                    { 16, new Tuple<int, int>(4, 4) },\n                };\n\n    IDictionary<string, object> default_values = new Dictionary<string, object>()\n    {\n        {\"platform-node-count\",1},\n        {\"platform-node-processor-count\",0},\n        {\"platform-node-processor-t_dFMA\",(double)0.250},\n        {\"platform-node-memory-latency\",256},\n        {\"platform-node-processor-core-cache_L1d-size\",16},\n        {\"platform-node-processor-core-cache_L2-size\",128},\n        {\"platform-node-processor-cache_L3-size\",768},\n        {\"platform-node-processor-core-cache_L1d-line_size\",64},\n        {\"platform-node-processor-core-cache_L2-line_size\",64},\n        {\"platform-node-processor-cache_L3-line_size\",64},\n        {\"platform-node-processor-core-cache_L1d-latency\",10},  //ns\n        {\"platform-node-processor-core-cache_L2-latency\",28},   //ns\n        {\"platform-node-processor-cache_L3-latency\",140},  //ns\n\t\t{\"platform-node-processor-core-clock\",1},\n        {\"platform-interconnection-startup_time\",10}, //ms\n        {\"platform-interconnection-bandwidth\",1000}, //Mbps\n        {\"min_M\",0},\n        {\"min_N\",0},\n        {\"min_P\",0},\n        {\"max_M\",(int)Math.Pow(10,3)},\n        {\"max_N\",(int)Math.Pow(10,3)},\n        {\"max_P\",(int)Math.Pow(10,3)}\n    };\n\n    static private int WORD_SIZE = 8;\n\n    public object execution_time\n    {\n        get\n        {\n            double T_exchange, T_multiply, T_reduce, T_rearrange, T_gemm;\n\n            int NODE_COUNT = (int)argument[\"platform-node-count\"]; NODE_COUNT = NODE_COUNT > 0 ? NODE_COUNT : (int)default_values[\"platform-node-count\"]; // number of nodes in the cluster\n            int cache_size_L1d = (int)argument[\"platform-node-processor-core-cache_L1d-size\"]; cache_size_L1d = cache_size_L1d > 0 ? cache_size_L1d : (int)default_values[\"platform-node-processor-core-cache_L1d-size\"];\n            int cache_size_L2 = (int)argument[\"platform-node-processor-core-cache_L2-size\"]; cache_size_L2 = cache_size_L2 > 0 ? cache_size_L2 : (int)default_values[\"platform-node-processor-core-cache_L2-size\"];\n            int cache_size_L3 = (int)argument[\"platform-node-processor-cache_L3-size\"]; cache_size_L3 = cache_size_L3 > 0 ? cache_size_L3 : (int)default_values[\"platform-node-processor-cache_L3-size\"];\n            int CLOCK = (int)argument[\"platform-node-processor-core-clock\"]; CLOCK = CLOCK > 0 ? CLOCK : (int)default_values[\"platform-node-processor-core-clock\"];\n            int t_s = (int)argument[\"platform-interconnection-startup_time\"]; t_s = t_s < int.MaxValue ? t_s : (int)default_values[\"platform-interconnection-startup_time\"];\n            int interconnection_bandwidth = (int)argument[\"platform-interconnection-bandwidth\"]; interconnection_bandwidth = interconnection_bandwidth > 0 ? interconnection_bandwidth : (int)default_values[\"platform-interconnection-bandwidth\"];\n            int min_M = (int)argument[\"min_M\"]; min_M = min_M > 0 ? min_M : (int)default_values[\"min_M\"]; // rows of matrices A and C\n            int min_N = (int)argument[\"min_N\"]; min_N = min_N > 0 ? min_N : (int)default_values[\"min_N\"]; // columns of matrix A and rows of matrix B\n            int min_P = (int)argument[\"min_P\"]; min_P = min_P > 0 ? min_P : (int)default_values[\"min_P\"]; // colunms of matrix B and C\n            int max_M = (int)argument[\"max_M\"]; max_M = max_M < int.MaxValue ? max_M : (int)default_values[\"max_M\"]; // rows of matrices A and C\n            int max_N = (int)argument[\"max_N\"]; max_N = max_N < int.MaxValue ? max_N : (int)default_values[\"max_N\"]; // columns of matrix A and rows of matrix B\n            int max_P = (int)argument[\"max_P\"]; max_P = max_P < int.MaxValue ? max_P : (int)default_values[\"max_P\"]; // colunms of matrix B and C\n            int processor_count = (int)argument[\"platform-node-processor-count\"]; processor_count = processor_count > 0 ? processor_count : (int)default_values[\"platform-node-processor-count\"];\n            int core_per_processor_count = (int)argument[\"platform-node-processor-core-count\"]; core_per_processor_count = core_per_processor_count > 0 ? core_per_processor_count : (int)default_values[\"platform-node-processor-core-count\"];\n            double t_multiplyadd = (double)argument[\"platform-node-processor-t_dFMA\"]; t_multiplyadd = t_multiplyadd < (double)decimal.MaxValue ? t_multiplyadd : (double)default_values[\"platform-node-processor-t_dFMA\"];\n            int t_L1 = (int)argument[\"platform-node-processor-core-cache_L1d-latency\"]; t_L1 = t_L1 < int.MaxValue ? t_L1 : (int)default_values[\"platform-node-processor-core-cache_L1d-latency\"];\n            int t_L2 = (int)argument[\"platform-node-processor-core-cache_L2-latency\"]; t_L2 = t_L2 < int.MaxValue ? t_L2 : (int)default_values[\"platform-node-processor-core-cache_L2-latency\"];\n            int t_L3 = (int)argument[\"platform-node-processor-cache_L3-latency\"]; t_L3 = t_L3 < int.MaxValue ? t_L3 : (int)default_values[\"platform-node-processor-cache_L3-latency\"];\n            int t_M = (int)argument[\"platform-node-memory-latency\"]; t_M = t_M < int.MaxValue ? t_M : (int)default_values[\"platform-node-memory-latency\"];\n            int C_L1 = (int)argument[\"platform-node-processor-core-cache_L1d-line_size\"]; C_L1 = C_L1 > 0 ? C_L1 : (int)default_values[\"platform-node-processor-core-cache_L1d-line_size\"];\n            int C_L2 = (int)argument[\"platform-node-processor-core-cache_L2-line_size\"]; C_L2 = C_L2 > 0 ? C_L2 : (int)default_values[\"platform-node-processor-core-cache_L2-line_size\"];\n            int C_L3 = (int)argument[\"platform-node-processor-cache_L3-line_size\"]; C_L3 = C_L3 > 0 ? C_L3 : (int)default_values[\"platform-node-processor-cache_L3-line_size\"];\n\n\n            int M = (min_M + max_M) / 2;\n            int N = (min_N + max_N) / 2;\n            int P = (min_P + max_P) / 2;\n            int X = topology[NODE_COUNT].Item1; // number of rows of processing nodes \n            int Y = topology[NODE_COUNT].Item2; // number of columns of processing nodes\n            int m_a = M / (X * 2); // divide M/X\n            int n = N / (Y * 2);   // divide N/Y\n            int p_b = P / (X * 2); // divide P/X\n            int m_c = M / (X * 2); // divide M/X\n            int p_c = P / (Y * 2); // divide P/Y\n            int C = processor_count * core_per_processor_count; // number of processing cores \n            int b_1 = (int)Math.Truncate(Math.Sqrt(cache_size_L1d));\n            int b_2 = (int)Math.Truncate(Math.Sqrt(cache_size_L2));\n            int b_3 = (int)Math.Truncate(Math.Sqrt(cache_size_L3));\n            int t_w = WORD_SIZE /interconnection_bandwidth;\n\n            T_exchange = t_s + 2 * p_b * n * WORD_SIZE * t_w;\n\n            T_reduce = (X * Y * t_s + 2 * M * P * WORD_SIZE * t_w) * (double)Math.Log(Y);\n\n            T_rearrange = (t_s + 4 * M * P * WORD_SIZE * t_w) * (double)Math.Log(X * Y);\n\n            double M1 = M / X;\n            double N1 = N / Y;\n            double P1 = P / X;\n\n            T_multiply = (((M1 * N1 * P1) / C) * t_multiplyadd + 3 * (t_L1 + (t_L2 / (b_1 * C_L1)) + (t_L3 / (b_2 * C_L2)) + (t_M / (b_3 * C_L3)))) / CLOCK;\n\n            T_gemm = (T_exchange + T_multiply) * X + T_reduce + T_rearrange;\n\n            return (T_gemm/Math.Pow(10,9));\n        }\n    }\n\n}\n\n\n\n";

            var compParms = new CompilerParameters { GenerateExecutable = false, GenerateInMemory = true };
            var csProvider = new CSharpCodeProvider();
            CompilerResults compilerResults = csProvider.CompileAssemblyFromSource(compParms, sourceCode);

            object argument_calculator = compilerResults.CompiledAssembly.CreateInstance(class_id);

            PropertyInfo p = argument_calculator.GetType().GetProperty("Argument");

            p.SetValue(argument_calculator, argument_final_value, null);

            PropertyInfo p1 = argument_calculator.GetType().GetProperty("execution_time");

            object o = null;

            try
            {

                o = p1.GetValue(argument_calculator, null);

            }
            catch (Exception e)
            {
                Console.WriteLine();
            }

            return o;
        }
    }
}

