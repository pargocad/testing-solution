﻿using System;
using System.IO;
using br.ufc.pargo.hpe.backend.DGAC.utils;

namespace GenerateProject
{
    class MainClass
    {
        private const string ec2_family_base = "compute";
        private const string ec2_type_base = "C4";
        private const string ec2_model_base = "Large";

        static string BASE_PATH = Path.Combine(Constants.HPCShelf_HOME, "Case Study - Alite/workspace");

        public static void Main(string[] args)
        {
            string ec2_family = args[0];
            string ec2_type = args[1];
            string ec2_model = args[2];

            string path_new = BASE_PATH.Replace(ec2_family_base, ec2_family).Replace(ec2_type_base, ec2_type).Replace(ec2_model_base, ec2_model);

            if (Directory.Exists(path_new))
            {
                Console.WriteLine(path_new + " already exist !");
                return;
            }

            // Create directory
            Directory.CreateDirectory(path_new);

            // .settings
            DirectoryCopy(BASE_PATH + Path.DirectorySeparatorChar + ".settings", path_new + Path.DirectorySeparatorChar + ".settings", true);

            // .project file
            string dotproject_contents_base = File.ReadAllText(BASE_PATH + Path.DirectorySeparatorChar + ".project");
            string dotproject_contents_new = dotproject_contents_base.Replace(ec2_family_base, ec2_family).Replace(ec2_type_base, ec2_type).Replace(ec2_model_base, ec2_model);
            File.WriteAllText(path_new + Path.DirectorySeparatorChar + ".project", dotproject_contents_new);

            // .hpe file
            string[] hpe_file_name_base_parts = BASE_PATH.Split('.');
            string hpe_file_name_base = hpe_file_name_base_parts[hpe_file_name_base_parts.Length - 1] + ".hpe";
            string hpe_file_name_new = hpe_file_name_base.Replace(ec2_family_base, ec2_family).Replace(ec2_type_base, ec2_type).Replace(ec2_model_base, ec2_model);

            string dothpe_contents_base = File.ReadAllText(BASE_PATH + Path.DirectorySeparatorChar + hpe_file_name_base);
            string dothpe_contents_new = dothpe_contents_base.Replace(ec2_family_base, ec2_family).Replace(ec2_type_base, ec2_type).Replace(ec2_model_base, ec2_model);
            File.WriteAllText(path_new + Path.DirectorySeparatorChar + hpe_file_name_new, dothpe_contents_new);

            // build.xml
            string build_xml_contents_base = File.ReadAllText(BASE_PATH + Path.DirectorySeparatorChar + "build.xml");
            string build_xml_contents_new = build_xml_contents_base.Replace(ec2_family_base, ec2_family).Replace(ec2_type_base, ec2_type).Replace(ec2_model_base, ec2_model);
            File.WriteAllText(path_new + Path.DirectorySeparatorChar + "build.xml", build_xml_contents_new);

            Console.WriteLine("END {0}", path_new);
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }

}