﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using br.ufc.mdcc.hpc.shelf.SAFe;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBase;
using br.ufc.mdcc.hpcshelf.gust.example.tc.DataTriangle;
using br.ufc.mdcc.hpcshelf.gust.graph.OutputFormat;
using br.ufc.mdcc.hpcshelf.gust.graph.VertexBasic;
using br.ufc.mdcc.hpcshelf.gust.port.environment.PortTypeDataSinkGraphInterface;
using br.ufc.pargo.hpe.backend.DGAC.utils;
using br.ufc.pargo.hpe.ports;
using gov.cca;
using gov.cca.ports;

namespace Run_TC
{
    // MainClass will make the role of the Application component.
    class MainClass : HShelfApplication
    {
        static string OUTPUT_FILE_PATH = System.Environment.GetFolderPath(System.Environment.SpecialFolder.UserProfile) + "/gust.output";//target to folder home

        public static void Main(string[] args)
        {
            string system_contract = File.ReadAllText("/home/heron/Dropbox/Copy/ufc_mdcc_hpc/HPC-Shelf/Case Study - Gust/Run-Gust-TC/TC_System.cc");
            string app_name = "triangle_count";
            AbstractFramework frw = new HShelfFramework(app_name, system_contract);

            // Through the frwServices object, this class makes the role of the application component
            Services appServices = frw.getServices("Me", "HShelfApplication", frw.createTypeMap());

            appServices.registerUsesPort(Constants.BUILDER_SERVICE_PORT_NAME, Constants.BUILDER_SERVICE_PORT_TYPE, new TypeMapImpl());
            ComponentID host_cid = appServices.getComponentID();
            BuilderService bsPort = (BuilderService)appServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);

            string workflowCRef = app_name + ".workflow";
            ComponentID s_cid = bsPort.getComponentID(workflowCRef);

            // CONNECT TO THE GO PORT OF THE WORKFLOW COMPONENT.
            string go_port_name = workflowCRef + "_" + Constants.GO_PORT_NAME;
            appServices.registerUsesPort(go_port_name, Constants.GO_PORT_TYPE, new TypeMapImpl());
            bsPort.connect(host_cid, go_port_name, s_cid, Constants.GO_PORT_NAME);
            Console.WriteLine("BEFORE GO PORT " + workflowCRef + " --- " + appServices.GetHashCode());
            GoPort go_port = (GoPort)appServices.getPort(go_port_name);
            Console.WriteLine("AFTER GO PORT " + workflowCRef + " --- " + appServices.GetHashCode());

			// CONNECT TO THE PROVENANCE PORT OF THE WORKFLOW COMPONENT.
		//	string provenance_port_name = workflowCRef + "_" + Constants.PROVENANCE_PORT_NAME;
         //   appServices.registerUsesPort(provenance_port_name, Constants.PROVENANCE_PORT_TYPE, new TypeMapImpl());
		//	bsPort.connect(host_cid, provenance_port_name, s_cid, Constants.PROVENANCE_PORT_NAME);
        //    ProvenancePort provenance_port = (ProvenancePort)appServices.getPort(provenance_port_name);
        //    provenance_port.start(2);
			
            Thread thread_receive_output = new Thread(() =>
            {
                IClientBase<IPortTypeDataSinkGraphInterface> output_data = (IClientBase<IPortTypeDataSinkGraphInterface>)appServices.getPort("output_data");
                IOutputFormat<IVertexBasic, IDataTriangle> graph_output_format = (IOutputFormat<IVertexBasic, IDataTriangle>)appServices.getPort("output_format");

                long t0 = (long)(DateTime.UtcNow - (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))).TotalMilliseconds;
                IOutputFormatInstance<IVertexBasic, IDataTriangle> gof = (IOutputFormatInstance<IVertexBasic, IDataTriangle>)graph_output_format.Instance;
                Console.WriteLine("IDataSinkGraph: START 1 output_file_path = {0}", OUTPUT_FILE_PATH);
                Console.WriteLine("output_data is null ? {0}", output_data == null);
                IPortTypeDataSinkGraphInterface pair_reader = output_data.Client;
				Console.WriteLine("pair_reader is null ? {0}", pair_reader == null);
				ConcurrentQueue<Tuple<object, int>> pairs = pair_reader.readPairs();
                Semaphore not_empty = pair_reader.NotEmpty;
                Console.WriteLine("IDataSinkGraph: BEFORE LOOP");
                Tuple<object, int> pair;
                //int sum = 0;
                do
                {
                    Console.WriteLine("IDataSinkGraph: LOOP -- BEFORE not_empty LOCK");
                    not_empty.WaitOne();
                    Console.WriteLine("IDataSinkGraph: LOOP -- AFTER not_empty LOCK");
                    pairs.TryDequeue(out pair);
                    if (pair.Item2 > 0)
                        gof.sendToFile(OUTPUT_FILE_PATH, pair.Item1);
                    //sum += pair.Item2;

                } while (pair.Item2 > 0);
                Console.WriteLine("IDataSink: FINISH");
                long t1 = (long)(DateTime.UtcNow - (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))).TotalMilliseconds;
                Console.WriteLine("Time processing: " + (t1 - t0) + " ms");
                File.AppendAllText(OUTPUT_FILE_PATH + ".time", (t1 - t0) + "" + System.Environment.NewLine);

            });

            thread_receive_output.Start();
			
            go_port.go();
			
            thread_receive_output.Join();

           // provenance_port.confirm();
        }


    }


/*
namespace Run_TC
{
	// MainClass will make the role of the Application component.
	class MainClass : HShelfApplication
	{
		public static void Main(string[] args)
		{
			string path_contracts = "/home/heron/Dropbox/Copy/ufc_mdcc_hpc/Hash-Programming-Environment/HPC-Shelf/Tests/Run-Gust-TC/";
			string path_architecture_file = Environment.GetEnvironmentVariable("SWL_ARCHITECTURE_FILE_LOCATION");

			// The contracts will be provided by explicit files. However, it is possible to put
			// the contracts inside the architectural code, or even infom a URL for download.
			IDictionary<string, string> contracts = new Dictionary<string, string>();

			for (int i = 0; i < p_ids.Length; i++)
				contracts[p_ids[i]] = File.ReadAllText(path_contracts + p_contracts[i]);

			for (int i = 0; i < c_ids.Length; i++)
				contracts[c_ids[i]] = File.ReadAllText(path_contracts + c_contracts[i]);

			string solution_architecture_xml = File.ReadAllText(path_architecture_file);

			AbstractFramework frw = new HShelfFramework(solution_architecture_xml, contracts);
			// 1. After the framework creation, the Workflow and Application components are instantiated. 
			//    For beginnint the orchestration, the Workflow component is waiting from the application,
			//    through the "swl_port", an object that will provide the orchestration code.
			// 2. It is not necessary a connection to a "builder service", since all the components
			//    and their bindings are specified in the architectural code.          

			// Through the frwServices object, this class makes the role of the application component
			Services appServices = frw.getServices("Me", "HShelfApplication", frw.createTypeMap());

			// Take the "swl_port".
			ISWLWorkflowBinding swl_port = (ISWLWorkflowBinding)appServices.getPort("swl_port");

			// Provide the orchestration code that will executed by the Workflow component.
			swl_port.Server = new SWLPortTypeImpl();
		}

		public class SWLPortTypeImpl : ISWLPortType
		{
			public string Workflow
			{
				get
				{
					string file_name = Environment.GetEnvironmentVariable("SWL_ORQUESTRATION_FILE_LOCATION");
					string swl_code = File.ReadAllText(file_name);
					return swl_code;
				}
			}
		}

		static string[] p_ids = new string[6] {
			"platform_SAFe",
			"platform_data_source",
			"platform_reduce_0",
			"platform_reduce_1",
			"platform_reduce_2",
			"platform_data_sink"
		};

		static string[] c_ids = new string[9] {
			"source",
			"sink",
			"reducer_0",
			"reducer_1",
			"reducer_2",
			"shuffler_0",
			"shuffler_1",
			"shuffler_2",
			"splitter_output"
		};

		static string[] p_contracts = new string[6] {
			"PlatformSAFe.cc",
			"PlatformDataSource.cc",
			"PlatformReduce_0.cc",
			"PlatformReduce_1.cc",
			"PlatformReduce_2.cc",
			"PlatformDataSink.cc"
		};

		static string[] c_contracts = new string[9] {
			"DataSource.cc",
			"DataSink.cc",
			"Reducer_0.cc",
			"Reducer_1.cc",
			"Reducer_2.cc",
			"Shuffler_0.cc",
			"Shuffler_1.cc",
			"Shuffler_2.cc",
			"SplitterOutput.cc"
		};

	}
*/
}
