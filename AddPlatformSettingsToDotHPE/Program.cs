﻿using System;
using System.IO;
using br.ufc.pargo.hpe.backend.DGAC.database;

namespace AddPlatformSettingsToDotHPE
{
    class MainClass
    {
        static string BASE_PATH = Path.Combine(Constants.HPCShelf_HOME, "Case Study - Alite/workspace");

        public static void Main(string[] args)
        {
            string hpe_file_name = Path.Combine(BASE_PATH, "br.ufc.mdcc.backend.ec2.impl.platform." + args[1] + ".EC2_" + args[2] + "_" + args[3] + "_impl", "EC2_" + args[2] + "_" + args[3] + "_impl.hpe");
            string module_data = File.ReadAllText(hpe_file_name);

            string platform_settings_contents = File.ReadAllText(Path.Combine(BASE_PATH, "br.ufc.mdcc.backend.ec2.impl.platform." + args[1] + ".EC2_" + args[2] + "_" + args[3] + "_impl", "src", "1.0.0.0", "platform.settings"));

			ComponentType c = LoaderApp.deserialize<ComponentType>(module_data);

            for (int k = 0; k < c.componentInfo.Length; k++)
            {
                object o = c.componentInfo[k];
                if (o is InterfaceType)
                {
                    InterfaceType it = (InterfaceType)o;

                    SourceType st = new SourceType();
                    st.deployType = st.sourceType = st.moduleName = "platform.settings";
                    st.file = new SourceFileType[1];
                    st.file[0] = new SourceFileType();
                    st.file[0].srcType = st.file[0].fileType = st.file[0].name = "platform.settings";
                    st.file[0].contents = platform_settings_contents;

                    int i;
                    for (i = 0; i < it.sources.Length; i++)
                    {
                        if (it.sources[i].deployType.Equals("platform.settings"))
                        {
                            // only update
                            it.sources[i] = st;
                            break;
                        }
                        else 
                        {
                            int found = -1;
                            for (int l = 0; l < it.sources[i].file.Length; l++)
                            {
                                if (it.sources[i].file[l].name.Equals("platform.settings"))
                                {
                                    found = l;
                                    break;
                                }
                            }

                            if (found > 0)
                            {
                                SourceFileType[] sft = new SourceFileType[it.sources[i].file.Length - 1];
                                int m, n=0;
                                for (m = 0; m < it.sources[i].file.Length - 1; m++)
                                    if (m != found)
                                        sft[n++] = it.sources[i].file[m];
                                it.sources[i].file = sft;
                            }

						}
                    }

                    if (i == it.sources.Length)
                    {
                        SourceType[] it_sources = new SourceType[it.sources.Length + 1];
                        int j;
                        for (j = 0; j < it.sources.Length; j++)
                            it_sources[j] = it.sources[j];
                        it_sources[j] = st;
						it.sources = it_sources;
					}
                }
            }

            string module_data_out = LoaderApp.serialize<ComponentType>(c);

            if (!File.Exists(hpe_file_name + ".backup.2"))
                File.WriteAllText(hpe_file_name + ".backup.2", module_data);

            File.WriteAllText(hpe_file_name, module_data_out);
        }
    }
}
