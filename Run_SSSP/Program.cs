﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using br.ufc.mdcc.hpc.shelf.SAFe;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBase;
using br.ufc.mdcc.hpcshelf.gust.example.sssp.DataSSSP;
using br.ufc.mdcc.hpcshelf.gust.graph.OutputFormat;
using br.ufc.mdcc.hpcshelf.gust.graph.VertexBasic;
using br.ufc.mdcc.hpcshelf.gust.port.environment.PortTypeDataSinkGraphInterface;
using br.ufc.pargo.hpe.backend.DGAC.utils;
using gov.cca;
using gov.cca.ports;

namespace TestSAFe
{
    // MainClass will make the role of the Application component.
    class MainClass : HShelfApplication
    {
        static string OUTPUT_FILE_PATH = System.Environment.GetFolderPath(System.Environment.SpecialFolder.UserProfile) + "/gust.output";//target to folder home

        public static void Main(string[] args)
        {
            string system_contract = File.ReadAllText("/home/heron/Dropbox/Copy/ufc_mdcc_hpc/HPC-Shelf/Case Study - Gust/Run-Gust-SSSP/SSSP_System.cc");
            string app_name = "sssp";
            AbstractFramework frw = new HShelfFramework(app_name, system_contract);

            // Through the frwServices object, this class makes the role of the application component
            Services appServices = frw.getServices("Me", "HShelfApplication", frw.createTypeMap());

            appServices.registerUsesPort(Constants.BUILDER_SERVICE_PORT_NAME, Constants.BUILDER_SERVICE_PORT_TYPE, new TypeMapImpl());
            ComponentID host_cid = appServices.getComponentID();
            BuilderService bsPort = (BuilderService)appServices.getPort(Constants.BUILDER_SERVICE_PORT_NAME);

            string workflowCRef = app_name + ".workflow";
            ComponentID s_cid = bsPort.getComponentID(workflowCRef);

            // CONNECT TO THE GO PORT OF THE WORKFLOW COMPONENT.
            string go_port_name = workflowCRef + "_" + Constants.GO_PORT_NAME;
            appServices.registerUsesPort(go_port_name, Constants.GO_PORT_TYPE, new TypeMapImpl());
            bsPort.connect(host_cid, go_port_name, s_cid, Constants.GO_PORT_NAME);
            Console.WriteLine("BEFORE GO PORT " + workflowCRef + " --- " + appServices.GetHashCode());
            GoPort go_port = (GoPort)appServices.getPort(go_port_name);
            Console.WriteLine("AFTER GO PORT " + workflowCRef + " --- " + appServices.GetHashCode());

            // CONNECT TO THE PROVENANCE PORT OF THE WORKFLOW COMPONENT.
            //  string provenance_port_name = workflowCRef + "_" + Constants.PROVENANCE_PORT_NAME;
            //   appServices.registerUsesPort(provenance_port_name, Constants.PROVENANCE_PORT_TYPE, new TypeMapImpl());
            //  bsPort.connect(host_cid, provenance_port_name, s_cid, Constants.PROVENANCE_PORT_NAME);
            //    ProvenancePort provenance_port = (ProvenancePort)appServices.getPort(provenance_port_name);
            //    provenance_port.start(2);

            Thread thread_receive_output = new Thread(() =>
            {
                IClientBase<IPortTypeDataSinkGraphInterface> output_data = (IClientBase<IPortTypeDataSinkGraphInterface>)appServices.getPort("output_data");
                IOutputFormat<IVertexBasic, IDataSSSP> graph_output_format = (IOutputFormat<IVertexBasic, IDataSSSP>)appServices.getPort("output_format");

                long t0 = (long)(DateTime.UtcNow - (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))).TotalMilliseconds;
                IOutputFormatInstance<IVertexBasic, IDataSSSP> gof = (IOutputFormatInstance<IVertexBasic, IDataSSSP>)graph_output_format.Instance;
                Console.WriteLine("IDataSinkGraph: START 1 output_file_path = {0}", OUTPUT_FILE_PATH);
                Console.WriteLine("output_data is null ? {0}", output_data == null);
                IPortTypeDataSinkGraphInterface pair_reader = output_data.Client;
                Console.WriteLine("pair_reader is null ? {0}", pair_reader == null);
                ConcurrentQueue<Tuple<object, int>> pairs = pair_reader.readPairs();
                Semaphore not_empty = pair_reader.NotEmpty;
                Console.WriteLine("IDataSinkGraph: BEFORE LOOP");
                Tuple<object, int> pair;
				
                int sum = 0;
				
                do
                {
                    Console.WriteLine("IDataSinkGraph: LOOP -- BEFORE not_empty LOCK");
                    not_empty.WaitOne();
                    Console.WriteLine("IDataSinkGraph: LOOP -- AFTER not_empty LOCK");
                    pairs.TryDequeue(out pair);
                    if (pair.Item2 > 0)
                        gof.sendToFile(OUTPUT_FILE_PATH, pair.Item1);
                    sum += pair.Item2;
                } while (pair.Item2 > 0);

                Console.WriteLine("IDataSink: FINISH sum={0}", sum);
                long t1 = (long)(DateTime.UtcNow - (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))).TotalMilliseconds;
                Console.WriteLine("Time processing: " + (t1 - t0) + " ms");
                File.AppendAllText(OUTPUT_FILE_PATH + ".time", (t1 - t0) + "" + System.Environment.NewLine);

            });

            thread_receive_output.Start();

            go_port.go();

            thread_receive_output.Join();

            // provenance_port.confirm();
        }
    }
}